﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {

    private Transform playerTrs;//主角位置
    public float moveSpeed = 2;//移动速度
    public Vector3 targetPos;
    public Quaternion targetRotation;

    void Awake() {
        playerTrs = GameObject.FindGameObjectWithTag(Tags.player).transform;
        targetPos = playerTrs.position + new Vector3(0f, 3f, -3f);
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        //位移跟随
        targetPos = playerTrs.position + new Vector3(0f, 3f, -3f);
        this.transform.position = Vector3.Lerp(this.transform.position, targetPos, moveSpeed * Time.deltaTime);
        
        //角度跟随（使摄像机一直注视着主角）
        //在主角和摄像机当前位置之间创建一个旋转角度，使用插值运算使摄像机注视主角
        targetRotation = Quaternion.LookRotation(playerTrs.position - this.transform.position);
        this.transform.rotation = Quaternion.Slerp(this.transform.rotation, targetRotation, moveSpeed * Time.deltaTime);
	}

    public IEnumerator Shake(float duration, float magnitude)
    {
        Vector3 originalPos = transform.position;

        float elapsed = 0.0f;

        while (elapsed < duration)
        {
            float x = Random.Range(originalPos.x, originalPos.x + 0.2f);
            float y = Random.Range(originalPos.y, originalPos.y + 0.2f);

            transform.localPosition = new Vector3(x, y, originalPos.z);
            elapsed = Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPos;
    }
}
