﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class CustomEventObject : UnityEvent<GameObject> { }


[CreateAssetMenu(fileName = "New Event", menuName = "STVR/Events/Create New Events", order = 51)]
public class EventObject : ScriptableObject
{
    private List<EventListenerManager> _listeners = new List<EventListenerManager>();
    private List<STVR.MeleeCombatKit.BehaviorEventSender> StateListener = new List<STVR.MeleeCombatKit.BehaviorEventSender>();
    public string EvName { private set; get; }

    private void OnEnable()
    {
        EvName = this.name;
    }

    public void RegisterEvent(EventListenerManager listener)
    {
        _listeners.Add(listener);
    }

    public void UnregisterEvent(EventListenerManager listener)
    {
        _listeners.Remove(listener);
    }

    public void RegisterEvent(STVR.MeleeCombatKit.BehaviorEventSender listener)
    {
        StateListener.Add(listener);
    }

    public void UnregisterEvent(STVR.MeleeCombatKit.BehaviorEventSender listener)
    {
        StateListener.Remove(listener);
    }

    //public void GlobalStateOccur(GameObject g, string hint)
    //{
    //    for (int i = 0; i < StateListener.Count; i++)
    //    {
    //        StateListener[i].m_Events.Find(x => x.mEvent.EvName.Equals(hint)).OnEventOccur(g);
    //    }
    //}

    //public void LocalStateOccur(GameObject g, string hint)
    //{
    //    StateListener.Find(x => x.aa.gameObject.Equals(g)).m_Events.Find(y => y.mEvent.EvName.Equals(hint)).OnEventOccur(g);
    //}

    public void GlobalOccur(GameObject g, string hint)
    {
        for (int i = 0; i < _listeners.Count; i++)
        {
            _listeners[i].m_Events.Find(x => x.mEvent.EvName.Equals(hint)).OnEventOccur(g);
        }
    }

    public void LocalOccur(GameObject g, string hint)
    {
        _listeners.Find(x => x.gameObject.Equals(g)).m_Events.Find(y => y.mEvent.EvName.Equals(hint)).OnEventOccur(g);
    }

    public void LocalOccur(GameObject g)
    {
        var p = _listeners.Find(x => x.gameObject.Equals(g)).m_Events;

        for (int i = 0; i < p.Count; i++)
        {
            p[i].OnEventOccur(g);
        }
    }

    public void Occurs(string hint, params GameObject[] target)
    {
        for (int i = 0; i < target.Length; i++)
        {
            if (_listeners[i].gameObject.Equals(target[i]))
            {
                _listeners[i].m_Events.Find(x => x.mEvent.EvName.Equals(hint)).OnEventOccur(target[i]);
            }
        }
    }
}
