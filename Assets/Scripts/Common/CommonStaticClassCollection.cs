﻿using UnityEngine;
using UnityEngine.Events;

namespace STVR.MeleeCombatKit
{

    #region Parameter Control

    public static class AnimatorParameter
    {
        public static int Grabbed = Animator.StringToHash("Grabbed");
        public static int MovementID = Animator.StringToHash("Movement");
        public static int NormalAttack = Animator.StringToHash("Normal Attack");
        public static int HeavyAttack = Animator.StringToHash("Heavy Attack");
        public static int Grab = Animator.StringToHash("Grab");
        public static int ComboValid = Animator.StringToHash("Combo Valid");
        public static int CatchTarget = Animator.StringToHash("Catch Target");
        public static int HitReact = Animator.StringToHash("Hit React");
        public static int Block = Animator.StringToHash("On Block");
        public static int TriggerBlock = Animator.StringToHash("Block");
    }

    public static class AnimatorState //need rework.
    {
        public static string MovementID = "Under Body.Movement.Walk";
        public static string NormalAttack1 = "Normal Attack.Normal Attack.Unarmed.W1";
        public static string NormalAttack2 = "Normal Attack.Normal Attack.Unarmed.W2";
        public static string NormalAttack3 = "Normal Attack.Normal Attack.Unarmed.W3";
        public static string DoThrown = "Upper Body.Hit React.Grabbed.GR2";
        public static string HitReact1 = "Upper Body.Hit React.Normal React.HR1";
        //public static int OnHeavyAtk1 = Animator.StringToHash("Heavy Attack.Heavy Attack.Unarmed.S1");
        //public static int OnHeavyAtk2 = Animator.StringToHash("Normal Attack.Heavy Attack.Unarmed.S2");
        //public static int OnHeavyAtk3 = Animator.StringToHash("Normal Attack.Heavy Attack.Unarmed.S3");
    }

    public static class EventObjectIdentifier
    {
        public static readonly string NormalAttack = "OnWeakAttack";
        public static readonly string HeavyAttack = "OnHeavyAttack";
        public static readonly string OnMove = "OnMove";
        public static readonly string OnValidCombo = "OnValidComboInput";
        public static readonly string OnInvalidCombo = "OnInvalidComboInput";
        public static readonly string OnCountCombo = "OnCountCombo";
        public static readonly string OnEndCombo = "OnEndCombo";
        public static readonly string OnStartCombo = "OnStartCombo";
        public static readonly string OnLockRotation = "OnLockRotation";
        public static readonly string OnLockMovement = "OnLockMovement";
        public static readonly string OnUnlockRotation = "OnUnlockRotation";
        public static readonly string OnUnlockMovement = "OnUnlockMovement";
        public static readonly string OnSwitchCombo = "OnSwitchCombo";
        public static readonly string OnGrabInitiate = "OnGrabInitiate";
        public static readonly string OnGrabSuccess = "OnGrabSuccess";
        public static readonly string OnGrabEnd = "OnGrabEnd";
        public static readonly string OnGrabThrow = "OnGrabThrow";
        public static readonly string OnHitReceived = "OnHitReceived";
        public static readonly string OnBlockStart = "OnBlockStart";
        public static readonly string OnBlockEnd = "OnBlockEnd";
        public static readonly string OnTriggerBlock = "OnBlockTrigger";
    }

    public static class AttackTransition //need rework. make it work automatically
    {
        public static int OnAtkTrans1 = Animator.StringToHash("Normal Attack.Null.Null -> Normal Attack.Normal Attack.Unarmed.W1");
        public static int OnAtkTrans2 = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W1 -> Normal Attack.Normal Attack.Unarmed.W2");
        public static int OnAtkTrans3 = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W2 -> Normal Attack.Normal Attack.Unarmed.W3");
        public static int OnHeavyAtkTrans1 = Animator.StringToHash("Normal Attack.Null.Null -> Normal Attack.Heavy Attack.Unarmed.S2");
        public static int OnHeavyAtkTrans2 = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W1 -> Normal Attack.Normal Attack.Unarmed.W2");
        public static int OnAtk1TransExit = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W1 -> Exit");
        public static int OnAtk2TransExit = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W2 -> Exit");
        public static int OnAtk3TransExit = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W3 -> Exit");
        public static int OnHeavyAtk1TransExit = Animator.StringToHash("Heavy Attack.Heavy Attack.Unarmed.S1 -> Exit");
        public static int OnHeavyAtk2TransExit = Animator.StringToHash("Normal Attack.Heavy Attack.Unarmed.S2 -> Exit");
        public static int OnHeavyAtk3TransExit = Animator.StringToHash("Normal Attack.Heavy Attack.Unarmed.S3 -> Exit");
    }

    public static class AttackState //need rework. make it work automatically
    {
        public static int OnAtkIdle = Animator.StringToHash("Normal Attack.Null.Null");
        public static int OnAtk1 = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W1");
        public static int OnAtk2 = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W2");
        public static int OnAtk3 = Animator.StringToHash("Normal Attack.Normal Attack.Unarmed.W3");
        public static int OnHeavyAtk1 = Animator.StringToHash("Heavy Attack.Heavy Attack.Unarmed.S1");
        public static int OnHeavyAtk2 = Animator.StringToHash("Normal Attack.Heavy Attack.Unarmed.S2");
        public static int OnHeavyAtk3 = Animator.StringToHash("Normal Attack.Heavy Attack.Unarmed.S3");
        public static int OnGrab1 = Animator.StringToHash("Action.Grab.G1");
    }

    public static class OICaller
    {
        public static int GrapTargetPosition = "GrabTargetPos".GetHashCode();
    }

    #endregion

    #region Interface Helper

    public static class ParameterHelper
    {
        public static void SetCurrentHealth(IHaveHealth Healthobj, float val, _HealthModifyType ModifyType = _HealthModifyType.Set)
        {
            Healthobj.SetCurrentHealth(val, ModifyType);
        }

        public static void UpdateMaxHealth(IHaveHealth HealthObj, float val)
        {
            HealthObj.UpdateMaxHealth(val);
        }

        public static float GetCurrentHealth(IHaveHealth HealthObj)
        {
            return HealthObj.CurrentHealth;
        }

        public static float GetMaxHealth(IHaveHealth HealthObj)
        {
            return HealthObj.MaxHeath;
        }

        public static float GetCurrentHealthPercentage(IHaveHealth HealthObj) //from 0 to 100, you know the gist.
        {
            return (HealthObj.CurrentHealth / HealthObj.MaxHeath) * 100f;
        }
    }

    #endregion

    #region Pools

    public sealed class WorldObjects
    {
        private static readonly WorldObjects instance = new WorldObjects();
        private static ObjectIdentifier[] _objIdentifier;
        private static GameObject[] _player;
        public static WorldObjects Instance => instance;

        public ObjectIdentifier[] ObjIdentifier
        {
            get
            {
                return _objIdentifier;
            }
        }

        public GameObject[] Player
        {
            get
            {
                return _player;
            }
        }

        static WorldObjects()
        {
            PoolObjects("ObjectIdentifier", out _objIdentifier);
            PoolObjects("Player", out _player);
        }

        private WorldObjects()
        {

        }

        private static void PoolObjects<T>(string tagToFind, out T[] arrayToFill) where T : Object
        {
            var z = GameObject.FindGameObjectsWithTag(tagToFind);
            arrayToFill = new T[z.Length];
            for (int i = 0; i < z.Length; i++)
            {
                if (typeof(T) != typeof(GameObject))
                    arrayToFill[i] = z[i].GetComponent<T>();
                else
                    arrayToFill[i] = z[i] as T;
            }
        }

        public void ResetPoolObjects<T>(T[] ObjectPool) where T : Object
        {
            for (int i = 0; i < ObjectPool.Length; i++)
            {
                ObjectPool[i] = null;
            }

            ObjectPool = null;
        }

        public GameObject GetObjectIdentifierObject(int Name, Transform hint) //get object with hint as reference. useful when try find local object.
        {
            return System.Array.Find(_objIdentifier, x => x.ObjectName.GetHashCode() == Name && x.transform.parent.Equals(hint)).gameObject;
        }

        public GameObject GetObjectIdentifierObject(string Name, Transform hint)
        {
            return System.Array.Find(_objIdentifier, x => x.ObjectName.Equals(Name) && x.transform.parent.Equals(hint)).gameObject;
        }

        public GameObject GetObjectIdentifierObject(int Name)
        {
            return System.Array.Find(_objIdentifier, x => x.ObjectName.GetHashCode() == Name).gameObject;
        }

        public GameObject GetObjectIdentifierObject(string Name)
        {
            return System.Array.Find(_objIdentifier, x => x.ObjectName.Equals(Name)).gameObject;
        }

    }

    #endregion

    #region Extensions

    public static class StaticTransformExtension
    {
        /// <summary>
        /// Change target parent to Parent, then change it local position to 0 (except Y Axis), then unparent it.
        /// </summary>
        /// <param name="Parent">Parent object for Target</param>
        /// <param name="Target">Target to change the Parent</param>
        public static void SetParentTo(this Transform Parent, Transform Target)
        {
            Target.SetParent(Parent);
            Vector3 parentTransform = new Vector3(0f, Target.transform.localPosition.y, 0f);
            Target.localPosition = parentTransform;
            Target.SetParent(null);
        }

        /// <summary>
        /// Change target parent to Parent, then change it local position to 0 (except Y Axis), then unparent it.
        /// </summary>
        /// <param name="Parent">Parent object for Targets</param>
        /// <param name="Target">Target to change the Parent</param>
        public static void SetTargetsParentTo(this Transform Parent, params Transform[] Targets)
        {
            for (int i = 0; i < Targets.Length; i++)
            {
                Targets[i].SetParent(Parent);
            }
        }

    }

    public static class AnimatorExtension
    {
        static WaitForSeconds ActiveDelay;
        static WaitForSeconds AsyncDelay;

        public static void ChangeIKPosition(this Animator anim, Transform targetPosition, AvatarIKGoal IKToChange, string IKNameWeight)
        {
            if (anim)
            {
                anim.SetIKPositionWeight(IKToChange, anim.GetFloat(IKNameWeight));
                anim.SetIKPosition(IKToChange, targetPosition.position);
            }
        }

        public static void ChangeIKHintPosition(this Animator anim, Transform targetPosition, AvatarIKHint IKToChange, string IKNameWeight)
        {
            if (anim)
            {
                anim.SetIKHintPositionWeight(IKToChange, anim.GetFloat(IKNameWeight));
                anim.SetIKHintPosition(IKToChange, targetPosition.position);
            }
        }

        /// <summary>
        /// Allow to play animation while root motion active with delay before and after.
        /// </summary>
        /// <param name="anim">Targeted animator.</param>
        /// <param name="activeDelayDuration">delay before root motion active.</param>
        /// <param name="DelayDuration">delay before root motion turn off.</param>
        /// <param name="AnimNameHash">hash name of animation clip to play</param>
        public async static void RootMotionActiveForDurationWhilePlayAnim(this Animator anim, float activeDelayDuration, float DelayDuration, int AnimNameHash)
        {
            ActiveDelay = new WaitForSeconds(activeDelayDuration);
            AsyncDelay = new WaitForSeconds(DelayDuration);
            anim.Play(AnimNameHash);
            await ActiveDelay;
            anim.applyRootMotion = true;
            await AsyncDelay;
            anim.applyRootMotion = false;
        }

        /// <summary>
        /// Allow to play animation while root motion active with delay before and after, with callback.
        /// </summary>
        /// <param name="anim">Targeted animator.</param>
        /// <param name="activeDelayDuration">delay before root motion active.</param>
        /// <param name="DelayDuration">delay before root motion turn off.</param>
        /// <param name="AnimNameHash">hash name of animation clip to play</param>
        /// <param name="callback">Callback Function on root motion.</param>
        public async static void RootMotionActiveForDurationWhilePlayAnim(this Animator anim, float activeDelayDuration, float DelayDuration, int AnimNameHash, UnityAction callback)
        {
            ActiveDelay = new WaitForSeconds(activeDelayDuration);
            AsyncDelay = new WaitForSeconds(DelayDuration);
            anim.Play(AnimNameHash);
            await ActiveDelay;
            anim.applyRootMotion = true;
            callback.Invoke();
            await AsyncDelay;
            anim.applyRootMotion = false;
        }

        /// <summary>
        /// Allow to play animation while root motion active with delay before and after.
        /// </summary>
        /// <param name="anim">Targeted animator.</param>
        /// <param name="activeDelayDuration">delay before root motion active.</param>
        /// <param name="DelayDuration">delay before root motion turn off.</param>
        /// <param name="AnimNameHash">hash name of animation clip to play</param>
        public async static void RootMotionActiveForDurationWhilePlayAnim(this Animator anim, float activeDelayDuration, float DelayDuration, string AnimNameHash)
        {
            ActiveDelay = new WaitForSeconds(activeDelayDuration);
            AsyncDelay = new WaitForSeconds(DelayDuration);
            anim.Play(AnimNameHash);
            await ActiveDelay;
            anim.applyRootMotion = true;
            await AsyncDelay;
            anim.applyRootMotion = false;
        }

        /// <summary>
        /// Allow to play animation while root motion active with delay before and after, with callback.
        /// </summary>
        /// <param name="anim">Targeted animator.</param>
        /// <param name="activeDelayDuration">delay before root motion active.</param>
        /// <param name="DelayDuration">delay before root motion turn off.</param>
        /// <param name="AnimNameHash">hash name of animation clip to play</param>
        /// <param name="callback">Callback Function on root motion.</param>
        public async static void RootMotionActiveForDurationWhilePlayAnim(this Animator anim, float activeDelayDuration, float DelayDuration, string AnimNameHash, UnityAction callback)
        {
            ActiveDelay = new WaitForSeconds(activeDelayDuration);
            AsyncDelay = new WaitForSeconds(DelayDuration);
            anim.Play(AnimNameHash);
            await ActiveDelay;
            anim.applyRootMotion = true;
            callback.Invoke();
            await AsyncDelay;
            anim.applyRootMotion = false;
        }
    }

    #endregion

}
