﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// a Helper class for find child object of a parent.
/// </summary>
public class ObjectIdentifier : MonoBehaviour
{
    [SerializeField] string m_ObjectName;
    public string ObjectName => m_ObjectName;


}
