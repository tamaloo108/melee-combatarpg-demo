﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace STVR.MeleeCombatKit
{
    public class BehaviorEventSender : StateMachineBehaviour
    {
        [SerializeField] [Range(0, 1f)] float SendAtNormalizedTime;
        public bool TriggerOnce;

        public List<EventObjectItem> m_Events;

        public ActorAnimation aa { private set; get; }


        int triggerCount = 0;

        bool isTriggerOnce()
        {
            if (TriggerOnce)
                return TriggerOnce && triggerCount == 0;
            else
                return !TriggerOnce || triggerCount > 0;
        }

        internal void UnregisterAllEvents()
        {
            for (int i = 0; i < m_Events.Count; i++)
            {
                m_Events[i]._Event.UnregisterEvent(this);
            }
        }

        //OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            m_Events.Find(x => x._Event.EvName.Equals(EventObjectIdentifier.OnCountCombo))._Event.LocalOccur(animator.gameObject, EventObjectIdentifier.OnCountCombo);
            m_Events.Find(x => x._Event.EvName.Equals(EventObjectIdentifier.OnInvalidCombo))._Event.LocalOccur(animator.gameObject, EventObjectIdentifier.OnInvalidCombo);
        }

        // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //Debug.Log(stateInfo.normalizedTime);
            if (stateInfo.normalizedTime >= SendAtNormalizedTime)
            {
                if (isTriggerOnce())
                {
                    m_Events.Find(x => x._Event.EvName.Equals(EventObjectIdentifier.OnValidCombo))._Event.LocalOccur(animator.gameObject, EventObjectIdentifier.OnValidCombo);
                    triggerCount++;
                }
            }
        }

        // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            m_Events.Find(x => x._Event.EvName.Equals(EventObjectIdentifier.OnInvalidCombo))._Event.LocalOccur(animator.gameObject, EventObjectIdentifier.OnInvalidCombo);
        }

        // OnStateMove is called right after Animator.OnAnimatorMove()
        //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that processes and affects root motion
        //}

        // OnStateIK is called right after Animator.OnAnimatorIK()
        //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        //{
        //    // Implement code that sets up animation IK (inverse kinematics)
        //}
    }
}