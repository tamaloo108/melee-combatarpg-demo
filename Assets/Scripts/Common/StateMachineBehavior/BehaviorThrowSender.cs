﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STVR.MeleeCombatKit
{
    public class BehaviorThrowSender : BehaviorEventBase
    {
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            for (int i = 0; i < _mEvents.Count; i++)
            {
                _mEvents[i]._Event.GlobalOccur(animator.gameObject, EventObjectIdentifier.OnGrabThrow);
            }
        }

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

        }

    }
}
