﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STVR.MeleeCombatKit
{
    public class BehaviorEventBase : StateMachineBehaviour
    {
        public List<EventObjectItem> _mEvents;

        protected virtual void ActivateLocalEvent(string name, GameObject g)
        {
            _mEvents.Find(x => x._Event.EvName.Equals(name))._Event.LocalOccur(g, name);
        }

        protected virtual void ActivateLocalEvents(GameObject g)
        {
            for (int i = 0; i < _mEvents.Count; i++)
            {
                _mEvents[i]._Event.LocalOccur(g, _mEvents[i]._Event.EvName);
            }
        }

        protected virtual void ActivateGlobalEvent(string name, GameObject g)
        {
            _mEvents.Find(x => x._Event.EvName.Equals(name))._Event.GlobalOccur(g, name);
        }

        protected virtual void ActivateGlobalEvents(GameObject g)
        {
            for (int i = 0; i < _mEvents.Count; i++)
            {
                _mEvents[i]._Event.GlobalOccur(g, _mEvents[i]._Event.EvName);
            }
        }
    }
}
