﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventListenerManager : MonoBehaviour
{
    //public EventObject mEvent;
    //public CustomEventObject response = new CustomEventObject();
    public List<EventListener> m_Events;

    private void OnEnable()
    {
        RegisterList();
    }

    private void OnDisable()
    {
        UnregisterList();
    }

    void RegisterList()
    {
        for (int i = 0; i < m_Events.Count; i++)
        {
            m_Events[i].mEvent.RegisterEvent(this);
        }
    }

    void UnregisterList()
    {
        for (int i = 0; i < m_Events.Count; i++)
        {
            m_Events[i].mEvent.UnregisterEvent(this);
        }
    }

}

[System.Serializable]
public class EventListener : PropertyAttribute
{
    public EventObject mEvent;
    public CustomEventObject response = new CustomEventObject();

    public void OnEventOccur(GameObject g)
    {
        response.Invoke(g);
    }

    public void AddResponse(UnityAction<GameObject> g)
    {
        response.AddListener(g);
    }

    public bool IsResponEmpty()
    {
        return response.GetPersistentEventCount() == 0 || response == null;
    }
}
