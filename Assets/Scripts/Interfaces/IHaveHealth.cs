﻿namespace STVR.MeleeCombatKit
{
    public interface IHaveHealth
    {
        float MaxHeath { get; set; }
        float CurrentHealth { get; set; }


        void UpdateMaxHealth(float nMaxHealth);
        void SetCurrentHealth(float val, _HealthModifyType type = _HealthModifyType.Set);
    }

    public enum _HealthModifyType
    {
        Set = 0,
        Increase = 1,
        Decrease = 2
    }
}