﻿namespace STVR.MeleeCombatKit
{
    public interface IHaveStats
    {
        float GrabDistance { get; }
        float NormalAttackDistance { get; }
        float HeavyAttackDistance { get; }
        float WeaponAttackDistance { get; }
        int BaseDamage { get; }
        int BaseDefense { get; }
    }
}