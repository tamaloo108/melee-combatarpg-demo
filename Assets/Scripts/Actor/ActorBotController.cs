﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Panda;

namespace STVR.MeleeCombatKit
{
    public class ActorBotController : ActorCombatController
    {
        [HideInInspector] Transform TargetOnSight;
        [SerializeField] float SearchRadius;
        [SerializeField] float MaxChaseDist = 20;
        [SerializeField] float MinChaseStop = 0.5f;
        [SerializeField] float TargetMovePredictPow = 3f;

        float moveValue = 0f;
        bool OnBout = false;
        NavMeshAgent _agent;
        Vector3 destination;

        protected override void Start()
        {
            base.Start();
            _agent = GetComponent<NavMeshAgent>();
        }

        public override float GetMoveMagnitude()
        {
            if (GetInputMode() == _InputMode.Player)
                return base.GetMoveMagnitude();
            else
                return moveValue;
        }

        #region Navigation Task

        [Task]
        bool SetDestination(Vector3 p)
        {
            moveValue = 1f;
            destination = p;
            _agent.destination = p;
            SetLocalEventOccur(EventObjectIdentifier.OnMove, gameObject);
            return true;
        }

        [Task]
        void SetRotation()
        {
            var dir = (GetHitbox().CurrentTarget.transform.position - transform.position);
            float toTarget = Vector3.Angle(this.transform.forward, transform.TransformVector(dir));
            _agent.transform.rotation = Quaternion.Slerp(_agent.transform.rotation, Quaternion.AngleAxis(toTarget, Vector3.up), Time.deltaTime * 5f);

            var angles = (_agent.transform.rotation.eulerAngles - Quaternion.AngleAxis(toTarget, Vector3.up).eulerAngles).magnitude;

            Debug.Log(angles);
            if (angles <= .71f)
            {
                Task.current.Succeed();
            }

        }

        [Task]
        bool InverseSetDestination(Vector3 p) //inverse the destination position, useful for implement flee from target behaviour.
        {
            var dir = (p - transform.position);
            _agent.destination = (transform.position - dir);
            return true;
        }

        [Task]
        bool WithinchaseStopDistance()
        {
            var a = (_agent.pathEndPosition - GetHitbox().CurrentTarget.transform.position).magnitude;
            if (a < MinChaseStop)
            {
                return true;
            }
            return false;
        }

        [Task]
        void StopMove()
        {
            isDoneMove();
            _agent.isStopped = true;
            var lastPath = _agent.nextPosition;
            _agent.destination = lastPath;
            _agent.isStopped = false;
            Task.current.Succeed();
        }

        [Task]
        void FleeFromTarget()
        {
            Vector3 targetDir = GetHitbox().CurrentTarget.transform.position - this.transform.position;

            float Heading = Vector3.Angle(this.transform.forward, transform.TransformVector(GetHitbox().CurrentTarget.transform.forward));
            float toTarget = Vector3.Angle(this.transform.forward, transform.TransformVector(targetDir));

            if ((toTarget < 90 && Heading > 20))
            {
                InverseSetDestination(GetHitbox().CurrentTarget.transform.position - Vector3.one * MinChaseStop);
                return;
            }

            float lookAhead = targetDir.magnitude / (_agent.speed + (_agent.speed / 2f));

            SetDestination((GetHitbox().CurrentTarget.transform.position + GetHitbox().CurrentTarget.transform.forward * lookAhead * TargetMovePredictPow) - Vector3.one * MinChaseStop);
            Task.current.Succeed();
        }

        [Task]
        void PursueTarget()
        {
            if (GetHitbox().CurrentTarget == null) return;
            Vector3 targetDir = GetHitbox().CurrentTarget.transform.position - this.transform.position;

            float Heading = Vector3.Angle(this.transform.forward, transform.TransformVector(GetHitbox().CurrentTarget.transform.forward));
            float toTarget = Vector3.Angle(this.transform.forward, transform.TransformVector(targetDir));

            if ((toTarget > 90 && Heading < 20))
            {
                SetDestination(GetHitbox().CurrentTarget.transform.position);
                return;
            }

            float lookAhead = targetDir.magnitude / (_agent.speed + (_agent.speed / 2f));

            SetDestination((GetHitbox().CurrentTarget.transform.position + GetHitbox().CurrentTarget.transform.forward * lookAhead * TargetMovePredictPow) - Vector3.one);
            Task.current.Succeed();
        }

        [Task]
        void WaitArrival()
        {
            var task = Task.current;
            float d = _agent.remainingDistance;
            if (!task.isStarting && _agent.remainingDistance <= _agent.stoppingDistance)
            {
                isDoneMove();
                task.Succeed();
            }
        }

        [Task]
        bool isDoneMove()
        {
            moveValue = 0f;
            return true;
        }

        [Task]
        bool SetMoveRandom()
        {
            var dest = transform.position + Random.insideUnitSphere * SearchRadius;
            SetDestination(dest);

            return true;
        }

        [Task]
        bool isWithinChaseRadius()
        {
            if (GetHitbox().CurrentTarget == null)
                return false;
            var dist = Vector3.Distance(GetHitbox().CurrentTarget.transform.position, transform.position);

            if (dist < MaxChaseDist)
                return true;
            else
                return false;
        }

        [Task]
        bool PickTargetFromRadius()
        {
            GetHitbox().GetTarget(SearchRadius);
            return true;
        }

        [Task]
        void ResetCurrentTarget()
        {
            GetHitbox().ResetTarget();
            Task.current.Succeed();
        }

        [Task]
        bool hasTarget()
        {
            return GetHitbox().CurrentTarget != null;
        }

        [Task]
        bool TargetSighted()
        {
            return TargetOnSight != null;
        }

        #endregion

        #region Combat Task

        [Task]
        bool isOkayToBout()
        {
            var h = ParameterHelper.GetCurrentHealthPercentage(this);
            return h > 80f;
        }

        [Task]
        bool isWithinStrikeRange()
        {
            var a = (GetHitbox().CurrentTarget.transform.position - _agent.transform.position).magnitude;
            return a <= MinChaseStop;

        }

        [Task]
        bool EndBout()
        {
            OnBout = false;
            return true;
        }

        [Task]
        bool BeginBout()
        {
            OnBout = true;
            return true;
        }

        [Task]
        bool isOnBout()
        {
            return OnBout;
        }

        [Task]
        void WaitUntilHasTarget()
        {
            if (GetHitbox().CurrentTarget != null)
            {
                Task.current.Succeed();
            }
        }

        [Task]
        bool isTimeToFlee()
        {
            var h = ParameterHelper.GetCurrentHealthPercentage(this);
            return h < 40f;
        }

        #endregion
    }
}