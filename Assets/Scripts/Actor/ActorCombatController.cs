﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STVR.MeleeCombatKit
{
    public class ActorCombatController : ActorController, IHaveHealth, IHaveStats
    {
        public _Allegiance Allegiance;
        [SerializeField] myParameter m_Parameter;
        [SerializeField] [Range(0f, 6f)] float ComboTimer = 1f;

        bool OnCombo = false;
        bool onBlock = false;
        bool EndCombo = false;
        public bool OnGrapple { private set; get; } //doing grapple
        public bool BeingGrappled { private set; get; } //when grappled
        Coroutine ComboCounter;
        WaitForSeconds ComboTimeWait;
        private HitboxDetector _hitbox;

        public bool ForceRotation { private set; get; }

        public mAttackType CurrentAttackInput { get; private set; }

        public int ComboCount { get; private set; }

        public float GrabDistance => m_Parameter.m_GrabDistance;
        public float NormalAttackDistance => m_Parameter.m_NormalAttackDistance;
        public float HeavyAttackDistance => m_Parameter.m_HeavyAttackDistance;
        public float WeaponAttackDistance => m_Parameter.m_WeaponAttackDistance;

        public int BaseDamage => m_Parameter.mBaseDamage;
        public int BaseDefense => m_Parameter.mBaseDefense;

        float _maxHealth;
        float _CurrentHealth;

        float IHaveHealth.MaxHeath { get => _maxHealth; set => m_Parameter.m_MaxHealth = value; }
        float IHaveHealth.CurrentHealth { get => _CurrentHealth; set => _CurrentHealth = value; }

        protected GameObject SightRadius;

        protected virtual void Start()
        {
            ComboTimeWait = new WaitForSeconds(0.1f);
            _maxHealth = m_Parameter.m_MaxHealth;
            _CurrentHealth = _maxHealth;

            _hitbox = GetComponent<HitboxDetector>();
            _hitbox.SetTeam(Allegiance);

        }

        public HitboxDetector GetHitbox()
        {
            return _hitbox;
        }

        public _InputMode GetInputMode()
        {
            return InputMode;
        }

        public override void InputHandle()
        {
            if (IsPlayerControl())
            {
                base.InputHandle();
                GetNormalAttackInput();
                GetHeavyAttackInput();
                GetGrabInput();
                GetBlockInput();
            }
        }



        public override void OnUnlockMovement(GameObject g)
        {
            base.OnUnlockMovement(g);
        }

        public override void OnUnlockRotation(GameObject g)
        {
            base.OnUnlockRotation(g);
        }

        public override void OnLockMovement(GameObject g)
        {
            base.OnLockMovement(g);
        }

        public override void DoForceRotation(CharacterController cc, float rotationSpeed)
        {
            if (_hitbox.CurrentTarget == null)
                base.DoForceRotation(cc, rotationSpeed);
            else
                cc.transform.rotation = GetRotation(_hitbox.CurrentTarget.transform);
        }

        public void OnReachEndCombo(GameObject g)
        {
            ComboCount = 0;
        }

        public void OnReachEndCombo()
        {
            ComboCount = 0;
        }

        public void IncreaseComboCount(GameObject g)
        {
            if (ComboCounter == null)
            {
                ComboCount = 0;
            }
            else
            {
                ComboCount++;
            }
        }

        public override void OnLockRotation(GameObject g)
        {
            base.OnLockRotation(g);
        }

        public void OnMeInitiateGrab(GameObject g)
        {
            OnGrapple = true;
            BeingGrappled = false;
        }

        public void OnBeingGrab(GameObject g)
        {
            if (OnGrapple) return;
            BeingGrappled = true;
        }

        public void OnGrabEnd(GameObject g)
        {
            BeingGrappled = false;
            OnGrapple = false;

        }

        private void SwitchComboCheck(mAttackType atkType)
        {
            if (CurrentAttackInput != atkType)
            {
                CurrentAttackInput = atkType;
                //SetGlobalEventOccur(EventObjectIdentifier.OnSwitchCombo, gameObject);
            }
        }

        protected virtual void GetBlockInput()
        {
            if (Input.GetButtonDown("Block"))
            {
                if (!onBlock)
                {
                    onBlock = true;
                    SetLocalEventOccur(EventObjectIdentifier.OnBlockStart, gameObject);
                }
            }
            else if (Input.GetButtonUp("Block"))
            {
                if (onBlock)
                {
                    onBlock = false;
                    SetLocalEventOccur(EventObjectIdentifier.OnBlockEnd, gameObject);
                }
            }
        }

        protected virtual void GetGrabInput()
        {
            if (Input.GetButtonDown("Grab"))
            {
                //Get Target
                _hitbox.GetTarget(GrabDistance);

                //ForceRotation = true;

                //switch attack type animation
                SwitchComboCheck(mAttackType.Grab);

                if (ComboCounter == null)
                {
                    ComboCounter = StartCoroutine(ComboCounterEnumerable(ComboTimer));
                }

                CurrentAttackInput = mAttackType.Grab;
                SetLocalEventOccur(EventObjectIdentifier.OnGrabInitiate, gameObject);
                _hitbox.SetDamaged();

                //Grab unique set
                if (_hitbox.CurrentTarget != null)
                {
                    StaticTransformExtension.SetParentTo(WorldObjects.Instance.GetObjectIdentifierObject(OICaller.GrapTargetPosition, transform).transform, _hitbox.CurrentTarget.transform);
                    SetLocalEventOccur(EventObjectIdentifier.OnGrabSuccess, gameObject, _hitbox.CurrentTarget.gameObject);
                }
                else
                {
                    SetLocalEventOccur(EventObjectIdentifier.OnGrabEnd, gameObject);
                }
            }
        }


        private void NormalAttackInputDown()
        {
            if (Input.GetButtonDown("Weak Attack"))
            {
                //Get Target
                _hitbox.GetTarget(NormalAttackDistance);

                ForceRotation = true;

                //switch attack type animation
                SwitchComboCheck(mAttackType.Normal);

                if (ComboCounter == null)
                {
                    ComboCounter = StartCoroutine(ComboCounterEnumerable());
                }

                CurrentAttackInput = mAttackType.Normal;
                SetLocalEventOccur(EventObjectIdentifier.NormalAttack, gameObject);
                _hitbox.SetDamaged();
            }
        }

        private void HeavyAttackInputDown()
        {
            if (Input.GetButtonDown("Heavy Attack"))
            {
                //Get Target
                _hitbox.GetTarget(HeavyAttackDistance);

                ForceRotation = true;

                //switch attack type animation
                SwitchComboCheck(mAttackType.Heavy);

                if (ComboCounter == null)
                {
                    ComboCounter = StartCoroutine(ComboCounterEnumerable());
                }

                CurrentAttackInput = mAttackType.Heavy;
                SetLocalEventOccur(EventObjectIdentifier.HeavyAttack, gameObject);
                _hitbox.SetDamaged();
            }
        }

        protected virtual void GetHeavyAttackInput()
        {
            HeavyAttackInputDown();
        }

        protected virtual void GetNormalAttackInput()
        {
            NormalAttackInputDown();
        }

        private IEnumerator ComboCounterEnumerable(float _time)
        {
            var time = _time;
            while (time > 0)
            {
                OnCombo = true;
                if (EndCombo)
                {
                    break;
                }

                yield return ComboTimeWait;
                time -= 0.1f;
            }

            if (OnGrapple)
            {
                SetLocalEventOccur(EventObjectIdentifier.OnGrabEnd, gameObject, _hitbox.CurrentTarget.gameObject);
            }

            ForceRotation = false;
            _hitbox.ResetTarget();
            ComboCount = 0;
            OnCombo = false;
            EndCombo = false;
            OnReachEndCombo(gameObject);
            ComboCounter = null;
            yield break;
        }

        private IEnumerator ComboCounterEnumerable()
        {
            var time = ComboTimer;
            while (time > 0)
            {
                OnCombo = true;
                if (EndCombo)
                {
                    break;
                }

                yield return ComboTimeWait;
                time -= 0.1f;
            }

            if (OnGrapple)
            {
                SetLocalEventOccur(EventObjectIdentifier.OnGrabEnd, gameObject, _hitbox.CurrentTarget.gameObject);
            }

            ForceRotation = false;
            _hitbox.ResetTarget();
            ComboCount = 0;
            OnCombo = false;
            EndCombo = false;
            OnReachEndCombo(gameObject);
            ComboCounter = null;
            yield break;
        }

        private void GetBotAttackInput()
        {
            SetLocalEventOccur(EventObjectIdentifier.NormalAttack, gameObject);
        }

        void IHaveHealth.UpdateMaxHealth(float nMaxHealth)
        {
            _maxHealth = nMaxHealth;
        }

        void IHaveHealth.SetCurrentHealth(float val, _HealthModifyType type)
        {
            var vals = _CurrentHealth;
            vals = type == _HealthModifyType.Decrease ? vals -= val : type == _HealthModifyType.Increase ? vals += val : vals = val;
            if (vals > _maxHealth && vals < 0)
                _CurrentHealth = vals;
        }
    }

    [Serializable]
    public struct myParameter
    {
        [Range(0f, 1000f)] public float m_MaxHealth;
        [Range(0f, 6f)] public float m_GrabDistance;
        [Range(0f, 6f)] public float m_NormalAttackDistance;
        [Range(0f, 6f)] public float m_HeavyAttackDistance;
        [Range(0f, 6f)] public float m_WeaponAttackDistance;
        [Range(0f, 255f)] public int mBaseDamage;
        [Range(0f, 255f)] public int mBaseDefense;

        public myParameter(float m_MaxHealth, float m_GrabDistance, float m_NormalAttackDistance, float m_HeavyAttackDistance, float m_WeaponAttackDistance, int mBaseDamage, int mBaseDefense)
        {
            this.m_MaxHealth = m_MaxHealth;
            this.m_GrabDistance = m_GrabDistance;
            this.m_NormalAttackDistance = m_NormalAttackDistance;
            this.m_HeavyAttackDistance = m_HeavyAttackDistance;
            this.m_WeaponAttackDistance = m_WeaponAttackDistance;
            this.mBaseDamage = mBaseDamage;
            this.mBaseDefense = mBaseDefense;
        }
    }

    public enum mAttackType
    {
        Normal = 0,
        Heavy = 1,
        Grab = 2,
        Special = 3
    }
}
