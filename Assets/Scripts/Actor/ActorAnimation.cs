﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace STVR.MeleeCombatKit
{
    public class ActorAnimation : MonoBehaviour
    {
        [SerializeField] int NormalAttackLayer;
        [SerializeField] int HeavyAttackLayer;

        Animator anim;

        bool OnAction = true;
        bool onSwitch = false;

        ActorCombatController tempCombatController;
        int tempComboCount;
        mAttackType tempAttackType;

        WaitForSeconds AsyncDelay;

        AnimatorStateInfo stateInfo;
        AnimatorTransitionInfo transinfo;

        private void Start()
        {
            anim = GetComponent<Animator>();
            AsyncDelay = new WaitForSeconds(1f);

        }

        public void GetStateTransInfo(int layer)
        {
            stateInfo = anim.GetCurrentAnimatorStateInfo(layer);
            transinfo = anim.GetAnimatorTransitionInfo(layer);

            CorrectIK(layer);

        }

        private void CorrectIK(int layer)
        {
            try
            {
                if (layer == NormalAttackLayer)
                {
                    if (stateInfo.fullPathHash == AttackState.OnAtk1)
                    {
                        //right hand
                        anim.ChangeIKPosition(tempCombatController.GetHitbox().CurrentTarget.GetIKTarget(mIKTargetName.Upperbody), AvatarIKGoal.RightHand, "IKRightHandWeight");
                    }
                    else if (stateInfo.fullPathHash == AttackState.OnAtk2)
                    {
                        //right hand, right elbow
                        anim.ChangeIKHintPosition(tempCombatController.GetHitbox().CurrentTarget.GetIKTarget(mIKTargetName.Upperbody), AvatarIKHint.RightElbow, "IKRightElbowWeight");
                    }
                    else if (stateInfo.fullPathHash == AttackState.OnAtk3)
                    {
                        //left hand
                        anim.ChangeIKPosition(tempCombatController.GetHitbox().CurrentTarget.GetIKTarget(mIKTargetName.Head), AvatarIKGoal.LeftHand, "IKLeftHandWeight");
                    }
                }
            }
            catch (Exception)
            {

            }
        }

        private void OnAnimatorIK(int layerIndex)
        {
            if (anim)
                GetStateTransInfo(layerIndex);
        }

        private void PlayAnimationAtCombo(int combo, mAttackType AttackType)
        {
            if (tempCombatController.OnGrapple)
            {
                switch (AttackType)
                {
                    case mAttackType.Normal:
                        anim.SetTrigger(AnimatorParameter.HeavyAttack);
                        break;

                    case mAttackType.Heavy:
                        anim.SetTrigger(AnimatorParameter.NormalAttack);
                        break;
                }

                return;
            }

            switch (AttackType)
            {
                case mAttackType.Normal:
                    if (combo % 2 == 0)
                    {
                        anim.Play(AttackState.OnAtk2);
                    }
                    else if (combo % 2 != 0)
                    {
                        anim.Play(AttackState.OnAtk1);
                    }
                    else
                    {
                        anim.SetTrigger(AnimatorParameter.NormalAttack);
                    }
                    break;

                case mAttackType.Heavy:
                    if (combo % 2 == 0)
                    {
                        anim.Play(AttackState.OnHeavyAtk2);
                    }
                    else if (combo % 2 != 0)
                    {
                        anim.Play(AttackState.OnHeavyAtk1);
                    }
                    else
                    {
                        anim.SetTrigger(AnimatorParameter.HeavyAttack);
                    }
                    break;
            }
        }

        private void GetComboCount(GameObject g)
        {
            if (tempCombatController == null)
                tempCombatController = g.GetComponent<ActorCombatController>();

            if (tempCombatController != null)
            {
                tempComboCount = tempCombatController.ComboCount;
                tempAttackType = tempCombatController.CurrentAttackInput;
            }
        }

        public void PlayHitReactAnim(GameObject g)
        {
            anim.Play(AnimatorState.HitReact1);
            anim.SetTrigger(AnimatorParameter.HitReact);
        }

        public void PlayGrabbedAnim(GameObject g)
        {
            GetComboCount(g);

            anim.SetBool(AnimatorParameter.Grabbed, tempCombatController.BeingGrappled);

        }

        public void PlayMoveAnim(GameObject g)
        {
            var _c = GetComponent<ActorCombatController>();
            if (_c.GetInputMode() == _InputMode.Player)
                anim.SetFloat(AnimatorParameter.MovementID, _c.GetMoveMagnitude());
            else
            {
                var cc = GetComponent<ActorBotController>();
                anim.SetFloat(AnimatorParameter.MovementID, cc.GetMoveMagnitude());
            }
        }

        public void PlayThrowAnim(GameObject g)
        {
            var c = GetComponent<ActorCombatController>().BeingGrappled;
            if (c)
            {
                anim.RootMotionActiveForDurationWhilePlayAnim(0.25f, 3f, AnimatorState.DoThrown, RotateIKToTarget);
            }
        }

        private void RotateIKToTarget()
        {
            if (anim)
            {
                var rot = WorldObjects.Instance.Player[0].transform.position - this.transform.position;
                rot.y = transform.position.y;
                transform.rotation = Quaternion.LookRotation(rot);
            }
        }

        public void OnSwitchingCombo(GameObject g)
        {
            onSwitch = true;
            GetComboCount(g);
        }

        public void ResetTrigger()
        {
            anim.ResetTrigger(AnimatorParameter.NormalAttack);
            anim.ResetTrigger(AnimatorParameter.HeavyAttack);
        }

        public void PlayHeavyAttackAnim(GameObject g)
        {
            GetComboCount(g);

            if (onSwitch)
            {
                PlayAnimationAtCombo(tempComboCount, mAttackType.Normal);
                onSwitch = false;
            }
            else
            {
                anim.SetTrigger(AnimatorParameter.HeavyAttack);
            }

        }

        public void PlayGrabAnim(GameObject g)
        {
            // anim.Play(AttackState.OnGrab1);
            anim.SetTrigger(AnimatorParameter.Grab);
        }

        public void PlayWeakAttackAnim(GameObject g)
        {
            GetComboCount(g);

            if (onSwitch)
            {
                PlayAnimationAtCombo(tempComboCount, mAttackType.Heavy);
                onSwitch = false;
            }
            else
            {
                anim.SetTrigger(AnimatorParameter.NormalAttack);
            }

        }

        public void PlayBlockStartAnim(GameObject g)
        {
            anim.SetBool(AnimatorParameter.Block, true);
        }

        public void PlayBlockEndAnim(GameObject g)
        {
            anim.SetBool(AnimatorParameter.Block, false);
        }

        public void PlayTriggerBlockAnim(GameObject g)
        {
            anim.SetTrigger(AnimatorParameter.TriggerBlock);
        }

        public void ComboValid(GameObject g)
        {
            OnAction = true;
            anim.SetBool(AnimatorParameter.ComboValid, true);
        }

        public void ComboInvalid(GameObject g)
        {
            anim.SetBool(AnimatorParameter.ComboValid, false);

            ResetTrigger();
        }

        public void ResetTrigger(GameObject g) //for observer receiver.
        {
            ResetTrigger();
        }

        public void SuccessGrabTarget(GameObject g)
        {
            anim.SetBool(AnimatorParameter.CatchTarget, true);
        }

        public void EndGrabTarget(GameObject g)
        {
            anim.SetBool(AnimatorParameter.CatchTarget, false);
        }

        public void ResetBehaviorState()
        {
            var d = anim.GetBehaviour<BehaviorEventSender>();
            d.UnregisterAllEvents();
        }

        #region Combo & Attack Transition Check

        internal bool isValidToAttackInput()
        {
            return anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackState.OnAtkIdle ||
                anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackState.OnAtk1 ||
                anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackState.OnAtk2 ||
                anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackState.OnAtk3 ||
                anim.GetCurrentAnimatorStateInfo(HeavyAttackLayer).fullPathHash == AttackState.OnHeavyAtk1 ||
                anim.GetCurrentAnimatorStateInfo(HeavyAttackLayer).fullPathHash == AttackState.OnHeavyAtk2;
        }

        internal bool isOnExitState()
        {
            return anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackTransition.OnAtk1TransExit ||
                anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackTransition.OnAtk2TransExit ||
                anim.GetCurrentAnimatorStateInfo(NormalAttackLayer).fullPathHash == AttackTransition.OnAtk3TransExit ||
                anim.GetCurrentAnimatorStateInfo(HeavyAttackLayer).fullPathHash == AttackTransition.OnHeavyAtk2TransExit ||
                anim.GetCurrentAnimatorStateInfo(HeavyAttackLayer).fullPathHash == AttackTransition.OnHeavyAtk1TransExit;
        }

        #endregion

    }
}
