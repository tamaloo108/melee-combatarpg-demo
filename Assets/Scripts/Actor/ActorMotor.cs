﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace STVR.MeleeCombatKit
{
    public class ActorMotor : MonoBehaviour
    {
        [SerializeField] [Range(0f, 30f)] float Speed = 4f;
        [SerializeField] [Range(0f, 30f)] float RotateSpeed = 4f;
        public bool isActive = false;
        CharacterController _cc;
        ActorCombatController _aControl;
        ActorAnimation _anim;
        // Start is called before the first frame update
        void Start()
        {
            Application.quitting += Application_quitting;
            _cc = GetComponent<CharacterController>();
            _aControl = GetComponent<ActorCombatController>();
            _anim = GetComponent<ActorAnimation>();
        }

        private void Application_quitting()
        {
            _anim.ResetBehaviorState();
        }

        private void ProceedMove()
        {
            if (_aControl.LockMovement) return;
            _cc.SimpleMove(_aControl.GetMove(transform.position, Speed));
        }

        private void ProceedRotation()
        {
            if (_aControl.LockRotation) return;

            if (_aControl.ForceRotation)
                _aControl.DoForceRotation(_cc, RotateSpeed * Time.deltaTime);
            else
                _aControl.DoRotation(_cc, RotateSpeed * Time.deltaTime);
        }

        // Update is called once per frame
        void Update()
        {
            if (!isActive) return;

            if (_anim.isOnExitState())
            {
                _aControl.OnReachEndCombo();
            }

            if (_anim.isValidToAttackInput() && !_anim.isOnExitState())
            {
                _aControl.InputHandle();
            }
            else
            {
                _anim.ResetTrigger();
            }

        }

        private void FixedUpdate()
        {
            ProceedRotation();
            ProceedMove();
        }
    }
}