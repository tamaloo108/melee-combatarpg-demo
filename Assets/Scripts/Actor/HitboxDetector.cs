﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace STVR.MeleeCombatKit
{
    public class HitboxDetector : MonoBehaviour
    {
        [SerializeField] EventObjectItem m_DamagedEvent;
        [SerializeField] List<IkTarget> IKTargets;
        [SerializeField] [Range(0, 10f)] float DetectRadius = 5f;
        //[SerializeField] float AttackDistance;
        [SerializeField] bool AutoLockNearest = true;
        [SerializeField] bool ManualLockOverrideAutoLock = false;

        List<HitboxDetector> Targets = new List<HitboxDetector>();
        public HitboxDetector CurrentTarget { private set; get; }
        Collider[] TargetBuffer;

        _Allegiance Team;

        float tempDistance;
        HitboxDetector tempTarget;

        private void Start()
        {
            TargetBuffer = new Collider[20];

        }

        public void SetTeam(_Allegiance team)
        {
            Team = team;
        }

        public _Allegiance GetTeam()
        {
            return Team;
        }

        public Transform GetIKTarget(mIKTargetName TargetIK)
        {
            return IKTargets.Find(x => x.TargetName == TargetIK).Target;
        }

        public void ResetTarget()
        {
            Targets = new List<HitboxDetector>();
            CurrentTarget = null;
        }

        public void SetDamaged()
        {
            try
            {
                m_DamagedEvent._Event.LocalOccur(CurrentTarget.gameObject, EventObjectIdentifier.OnHitReceived);
            }
            catch (System.Exception)
            {

            }
        }

        public void GetTarget(float mAttackDistance)
        {
            try
            {
                Targets = new List<HitboxDetector>();
                CurrentTarget = null;
                Physics.OverlapSphereNonAlloc(transform.position, DetectRadius, TargetBuffer);
                for (int i = 0; i < TargetBuffer.Length; i++)
                {
                    var temp = TargetBuffer[i].GetComponent<HitboxDetector>();
                    if (temp == null || temp.transform == this.transform) continue;
                    if (!Targets.Contains(temp))
                    {
                        Targets.Add(temp);
                    }
                }

            }
            catch
            {

            }

            if (Targets != null)
            {
                if (AutoLockNearest)
                {
                    tempDistance = mAttackDistance;
                    tempTarget = null;
                    for (int i = 0; i < Targets.Count; i++)
                    {
                        float dist = Vector3.Distance(transform.position, Targets[i].transform.position);
                        if (dist < tempDistance)
                        {
                            tempTarget = Targets[i];
                            tempDistance = dist;
                        }
                    }

                    if (tempTarget != null)
                    {
                        if (tempTarget.GetTeam() != GetTeam())
                            CurrentTarget = tempTarget;
                    }
                }

                if (ManualLockOverrideAutoLock)
                {
                    //wip
                }
            }
        }

        public void GetTarget()
        {
            try
            {
                Physics.OverlapSphereNonAlloc(transform.position, DetectRadius, TargetBuffer);
                for (int i = 0; i < TargetBuffer.Length; i++)
                {
                    var temp = TargetBuffer[i].GetComponent<HitboxDetector>();
                    if (temp == null || temp.transform == this.transform) continue;
                    if (!Targets.Contains(temp))
                    {
                        Targets.Add(temp);
                    }
                }

            }
            catch (System.Exception e)
            {

            }

            if (Targets != null)
            {
                if (AutoLockNearest)
                {
                    tempDistance = Mathf.Infinity;
                    tempTarget = null;
                    for (int i = 0; i < Targets.Count; i++)
                    {
                        float dist = Vector3.Distance(transform.position, Targets[i].transform.position);
                        if (dist < tempDistance)
                        {
                            tempTarget = Targets[i];
                            tempDistance = dist;
                        }
                    }

                    if (tempTarget != null)
                    {
                        CurrentTarget = tempTarget;
                    }
                }

                if (ManualLockOverrideAutoLock)
                {
                    //wip
                }
            }
        }


    }

    [System.Serializable]
    public struct IkTarget
    {
        public Transform Target;
        public mIKTargetName TargetName;
    }

    public enum mIKTargetName
    {
        Head = 0,
        Upperbody = 1,
        Lowerbody = 2,
        Shoulder = 3
    }
}