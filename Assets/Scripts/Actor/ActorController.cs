﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace STVR.MeleeCombatKit
{
    public class ActorController : MonoBehaviour
    {
        [Header("Events")]
        public List<EventObjectItem> Events;
        public _InputMode InputMode;

        Vector3 _RawMovement { get; set; }
        Vector3 _LerpMovement { get; set; }

        Vector2 _input;

        public bool LockRotation;
        public bool LockMovement;

        public virtual void InputHandle()
        {
            GetMovementInput();
        }

        protected bool IsPlayerControl()
        {
            return InputMode == _InputMode.Player;
        }

        public Vector3 GetMove(Vector3 mPosition, float spd)
        {
            return _RawMovement * spd;
        }

        #region Event Listener

        public virtual void OnUnlockRotation(GameObject g)
        {
            LockRotation = false;
        }

        public virtual void OnUnlockMovement(GameObject g)
        {
            LockMovement = false;
        }

        public virtual void OnLockRotation(GameObject g)
        {
            LockRotation = true;
        }

        public virtual void OnLockMovement(GameObject g)
        {
            LockMovement = true;
        }

        #endregion

        public Quaternion GetRotation(Transform target)
        {
            Vector3 targetDir = target.position - transform.position;
            targetDir.y = target.position.y;
            return Quaternion.LookRotation(targetDir);
        }

        public Quaternion GetRotation()
        {
            var rot = _RawMovement.normalized;
            return Quaternion.LookRotation(rot);
        }

        public virtual void DoRotation(CharacterController cc, float rotationSpeed)
        {
            if (_RawMovement != Vector3.zero)
                cc.transform.rotation = Quaternion.Slerp(cc.transform.rotation, GetRotation(), rotationSpeed);
        }

        public virtual void DoForceRotation(CharacterController cc, float rotationSpeed)
        {
            if (_RawMovement != Vector3.zero)
                cc.transform.rotation = Quaternion.Slerp(cc.transform.rotation, GetRotation(), rotationSpeed);
        }

        public virtual float GetMoveMagnitude()
        {
            return _RawMovement.sqrMagnitude;
        }

        private void GetMovementInput()
        {
            _input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
            _RawMovement = new Vector3(_input.x, 0f, _input.y);

            SetGlobalEventOccur(EventObjectIdentifier.OnMove, gameObject);
        }

        public virtual void SetGlobalEventOccur(string name, GameObject g)
        {
            Events.Find(x => x._Event.EvName.Equals(name))._Event.GlobalOccur(g, name);
        }

        public virtual void SetLocalEventOccur(string name, GameObject g)
        {
            Events.Find(x => x._Event.EvName.Equals(name))._Event.LocalOccur(g, name);
        }

        public virtual void SetLocalEventOccur(string name, params GameObject[] target)
        {
            for (int i = 0; i < target.Length; i++)
            {
                Events.Find(x => x._Event.EvName.Equals(name))._Event.LocalOccur(target[i], name);
            }
        }
    }

    public enum _InputMode
    {
        Player = 0,
        Bot = 1
    }

    public enum _Allegiance
    {
        Player = 0,
        Enemy = 1
    }

    [System.Serializable]
    public struct EventObjectItem
    {
        public EventObject _Event;
    }
}