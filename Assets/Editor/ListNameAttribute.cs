﻿using UnityEngine;
using UnityEditor;

namespace STVR.MeleeCombatKit.Editor
{
    [CustomPropertyDrawer(typeof(NamedArrayAttribute))]
    public class NamedArrayDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect rect, SerializedProperty property, GUIContent label)
        {
            base.OnGUI(rect, property, label);

            try
            {
                EditorGUI.ObjectField(rect, property, new GUIContent(((EventListener)attribute).mEvent.EvName));
            }
            catch
            {

                EditorGUI.ObjectField(rect, property, label);
            }
        }
    }

    public class NamedArrayAttribute : PropertyAttribute
    {
        public readonly string names;
        public NamedArrayAttribute(string names) { this.names = names; }
    }
}
